import React, { Component } from 'react';
import {
    Text,
    ScrollView,
    TouchableOpacity,
    View,
} from 'react-native';

import BikeListElem from '../BikeListElem';

import styles from '../../lib/styles';

export default class BikeList extends Component {
    loaded = false
    getBikes = () => {
        const {bikes, getBikes} = this.props;
        const bikeList = Object.values(bikes);
        if (!this.loaded && bikeList.length === 0) {
            this.loaded = true;
            getBikes();
            return [];
        }
        return bikeList;
    }
    render() {
        const bikes = this.getBikes();
        const {onBike, navigation} = this.props;
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <TouchableOpacity
                    style={{...styles.button}}
                    onPress={() => navigation.navigate('BikeCreate')}
                >
                    <Text>Create</Text>
                </TouchableOpacity>
                <ScrollView
                style={{flex: 1}}
                >
                    {bikes.map((bike) => <BikeListElem key={bike.id} bike={bike} onBike={onBike}/>)}
                </ScrollView>
            </View>
        )
    }
}