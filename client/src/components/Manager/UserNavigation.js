import React from 'react';

import {
    createBottomTabNavigator
} from 'react-navigation'

import UserEditorContainer from '../../containers/Manager/UserEditorContainer';
import RentalListContainer from '../../containers/Manager/RentalListContainer';

export default () =>
    createBottomTabNavigator({
        User: UserEditorContainer,
        Reservations: RentalListContainer,
    }, {
        initialRouteName: 'User'
    });