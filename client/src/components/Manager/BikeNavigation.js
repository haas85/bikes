import React from 'react';

import {
    createBottomTabNavigator
} from 'react-navigation'

import BikeEditorContainer from '../../containers/Manager/BikeEditorContainer';
import RentalListContainer from '../../containers/Manager/RentalListContainer';

export default () =>
    createBottomTabNavigator({
        Bike: BikeEditorContainer,
        Reservations: RentalListContainer,
    }, {
        initialRouteName: 'Bike'
    });
