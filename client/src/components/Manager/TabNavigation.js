import React from 'react';

import {
    createBottomTabNavigator
} from 'react-navigation'

import BikeListContainer from '../../containers/Manager/BikeListContainer';
import UserListContainer from '../../containers/Manager/UserListContainer';

export default () => {
    return createBottomTabNavigator({
        Bikes: (props) => <BikeListContainer {...props} onBike={(bike) => props.navigation.navigate('Bike', {bike})}/>,
        Users:  (props) => <UserListContainer {...props} onUser={(user) => props.navigation.navigate('User', {user})}/>,
    }, {
        initialRouteName: 'Bikes'
    });
}