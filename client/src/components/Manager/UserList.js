import React, { Component } from 'react';
import {
    Text,
    ScrollView,
    TouchableOpacity,
    View,
} from 'react-native';

import UserListElem from '../UserListElem';

import styles from '../../lib/styles';

export default class UserList extends Component {
    loaded = false
    getUsers = () => {
        const {users, getUsers} = this.props;
        const userList = Object.values(users);
        if (!this.loaded && userList.length === 0) {
            this.loaded = true;
            getUsers();
            return [];
        }
        return userList;
    }
    render() {
        const users = this.getUsers();
        const {onUser, navigation} = this.props;
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <TouchableOpacity
                    style={{...styles.button}}
                    onPress={() => {navigation.navigate('UserCreate')}}
                >
                    <Text>Create</Text>
                </TouchableOpacity>
                <ScrollView
                style={{flex: 1}}
                >
                    {users.map((user) => <UserListElem key={user.id} user={user} onUser={onUser}/>)}
                </ScrollView>
            </View>
        )
    }
}