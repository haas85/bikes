import React, { Component } from 'react';
import {
    Text,
    View,
} from 'react-native';

import {
    createBottomTabNavigator, createStackNavigator, createAppContainer
} from 'react-navigation'

import TabNavigation from './TabNavigation';
import BikeNavigation from './BikeNavigation';
import UserNavigation from './UserNavigation';

import BikeEditorContainer from '../../containers/Manager/BikeEditorContainer';
import UserEditorContainer from '../../containers/Manager/UserEditorContainer';

import styles from '../../lib/styles';

class SettingsScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Settings!</Text>
            </View>
        );
    }
}

export default class Manager extends Component {

    getStackNavigation = () => {
        return createAppContainer(createStackNavigator({
            List: {
                screen: TabNavigation(),
                navigationOptions: {
                    header: null,
                },
            },
            Bike: {
                screen: BikeNavigation(),
                navigationOptions: {
                    title: 'Bike',
                },
            },
            BikeCreate: {
                screen: BikeEditorContainer,
                navigationOptions: {
                    title: "Create"
                }
            },
            User: {
                screen: UserNavigation(),
                navigationOptions: {
                    title: 'User',
                },
            },
            UserCreate: {
                screen: UserEditorContainer,
                navigationOptions: {
                    title: "Create"
                }
            },
        }, {
            initialRouteName: 'List',
            }));
    }
    render() {
        const Screens = this.getStackNavigation();
        return (
            <View style={{ flex: 1, padding: 10}}>
                <Screens />
            </View>
        )
    }
}