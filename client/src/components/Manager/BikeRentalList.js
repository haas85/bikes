import React, { Component } from 'react';
import {
    ScrollView
} from 'react-native';

import UserRentalElem from '../UserRentalElem';

export default class BikeRentalList extends Component {
    componentDidMount() {
        this.getRentals();
        const {navigation} = this.props;
        navigation.addListener('willFocus', () => this.getRentals())
    }

    getRentals = () => {
        const {getRentals, navigation} = this.props;
        const bike = navigation.getParam('bike');
        getRentals({bike: bike.id});
    }
    render() {
        const {rentals} = this.props;
        const _rentals = Object.values(rentals);
        return (
            <ScrollView
            style={{flex: 1}}
            >
                {_rentals.map((rental) => <UserRentalElem key={rental.id} rental={rental}/>)}
            </ScrollView>
        )
    }
}