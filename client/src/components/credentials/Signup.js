import React, { Component } from 'react';
import {
    Text,
    TextInput,
    View,
    Modal,
    TouchableOpacity,
    TouchableHighlight, Alert
} from 'react-native'

import validator from 'validator';

import styles from '../../lib/styles';

export default class Login extends Component {
    state = {
        mail: '',
        password: '',
        password2: '',
    }

    getError = () => {
        const {mail, password, password2} = this.state;
        if (mail !== '' && !validator.isEmail(mail)) {
            return 'Please insert a valid mail format';
        }

        if (password !== '' && password.length < 6) {
            return 'Passwords too short';
        }

        if (password !== '' && password2 !== '' && password !== password2) {
            return 'Passwords do not match';
        }
        return false;
    }

    signUp = () => {
        const {mail, password} = this.state;
        this.props.onSignup(mail, password).then(() => {
            Alert.alert(
                'Created!',
                'Account Created! Please login at login screen',
                [
                    {text: 'OK', onPress: () => this.props.navigation.goBack()},
                ],
                {cancelable: false},
            );
        }).catch(() => {
            Alert.alert(
                'Error',
                'Error creating account',
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
        })
    }

    render() {
        const {mail, password, password2} = this.state;
        const error = this.getError();
        const disabled = !!error || mail === '' || password === '' || password2 === '';

        return (
            <View style={{ flex: 1, padding: 10}}>
                <TextInput
                    placeholder='Email'
                    textContentType="emailAddress"
                    style={{...styles.input, ...styles.marginBottom}}
                    onChangeText={(text) => this.setState({mail: text.toLowerCase()})}
                    value={mail}
                />
                <TextInput
                    placeholder='Password'
                    textContentType="password"
                    secureTextEntry={true}
                    style={{...styles.input, ...styles.marginBottom}}
                    onChangeText={(text) => this.setState({password: text})}
                    value={password}
                />
                <TextInput
                    placeholder='Repeat Password'
                    textContentType="password"
                    secureTextEntry={true}
                    style={{...styles.input, ...styles.marginBottom}}
                    onChangeText={(text) => this.setState({password2: text})}
                    value={password2}
                />
                <View style={{margin:7}} />
                {error &&
                <View style={{margin:7}} >
                    <Text style={{color: 'red'}}>{error}</Text>
                </View>
                }
                <TouchableOpacity
                    style={{...styles.button, ...(disabled ? styles.disabled : {})}}
                    onPress={this.signUp}
                    disabled={disabled}
                ><Text>SignIn In</Text></TouchableOpacity>
            </View>
        )
    }
}