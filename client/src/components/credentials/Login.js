import React, { Component } from 'react';
import {
    Text,
    TextInput,
    View,
    Alert,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';

import styles from '../../lib/styles';
import validator from 'validator'

export default class Login extends Component {
    state = {
        mail: '',
        password: ''
    }

    getError = () => {
        const {mail, password} = this.state;
        if (mail !== '' && !validator.isEmail(mail)) {
            return 'Please insert a valid mail format';
        }

        if (password !== '' && password.length < 6) {
            return 'Passwords too short';
        }

        return false;
    }

    login = () => {
        const {mail, password} = this.state;
        this.props.onLogin(mail, password).then((data) => {
            AsyncStorage.setItem('@BIKE:USER', JSON.stringify(data));
        })
            .catch(() => {
            Alert.alert(
                'Error',
                'Error on log in',
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
        });
    }

    render() {
        const {mail, password} = this.state;
        const error = this.getError();
        const disabled = !!error || mail === '' || password === '';
        return (
            <View style={{ flex: 1, padding: 10}}>
                <TextInput
                    placeholder='Email'
                    textContentType="emailAddress"
                    style={{...styles.input, ...styles.marginBottom}}
                    onChangeText={(text) => this.setState({mail: text.toLowerCase()})}
                    value={mail}
                />
                <TextInput
                    placeholder='Password'
                    textContentType="password"
                    secureTextEntry={true}
                    style={{...styles.input, ...styles.marginBottom}}
                    onChangeText={(text) => this.setState({password: text})}
                    value={password}
                />
                <View style={{margin:7}} />
                {error &&
                <View style={{margin:7}} >
                    <Text style={{color: 'red'}}>{error}</Text>
                </View>
                }
                <TouchableOpacity
                    style={{...styles.button, ...(disabled ? styles.disabled : {})}}
                    onPress={this.login}
                    disabled={disabled}
                ><Text>Log In</Text></TouchableOpacity>
                <TouchableOpacity
                    style={{...styles.secondary, ...styles.marginTop}}
                    onPress={() => this.props.navigation.navigate('Signup')}
                ><Text>Sign In</Text></TouchableOpacity>
            </View>
        )
    }
}