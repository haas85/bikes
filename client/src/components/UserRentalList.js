import React, { Component } from 'react';
import {
    ScrollView,
} from 'react-native'

import BikeRentalElem from './BikeRentalElem';

export default class UserRentalList extends Component {
    componentDidMount() {
        this.getRentals();
        const {navigation} = this.props;
        navigation.addListener('willFocus', () => this.getRentals())
    }

    getRentals = () => {
        const {getRentals, navigation} = this.props;
        const user = navigation.getParam('user');
        getRentals({user: (user || window.user).id});
    }

    onRental = (rental) => {
        const {onRental = () => {}} = this.props;
        onRental(rental)
    }

    render() {
        const {rentals} = this.props;
        const _rentals = Object.values(rentals);
        return (
            <ScrollView
            style={{flex: 1}}
            >
                {_rentals.map((rental) => <BikeRentalElem key={rental.id} rental={rental} onRental={this.onRental}/>)}
            </ScrollView>
        )
    }
}