import React, { Component } from 'react';
import moment from 'moment';
import {
    Text,
    TouchableOpacity,
    Image,
    View, StyleSheet,
} from 'react-native';

import Average from './Average';

import styles from '../lib/styles';

const UserStyles = StyleSheet.create({
    root: {
        width: '100%',
        flexDirection: 'column',
        padding: 5,
        borderBottomColor: 'grey',
        borderBottomWidth: 2
    },
    info: {
        flex: 1,
        flexDirection: 'column'
    }
});

export default class UserRentalElem extends Component {
    render() {
        const {rental, onRental = () => {}} = this.props;
        const {mail} = rental.user;
        return (
            <TouchableOpacity
                style={UserStyles.root}
                onPress={() => onRental(rental.user)}
            >
                <View style={ UserStyles.info}>
                    <Text style={styles.marginBottom}>{mail}</Text>
                    <Text>From: {moment(rental.initial).format('MM/DD/YYYY')}</Text>
                    <Text>To: {moment(rental.end).format('MM/DD/YYYY')}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}