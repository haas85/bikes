import React, {Component} from 'react';

import {
    View,
} from 'react-native';

import {
    createStackNavigator, createAppContainer
} from 'react-navigation';

import TabNavigation from './TabNavigation';
import BikeReservation from './BikeReservation';
import ReservationView from './ReservationView';

export default class User extends Component {
    getStackNavigation = () => {
        return createAppContainer(createStackNavigator({
            List: {
                screen: TabNavigation(),
                path: 'list',
                navigationOptions: {
                    header: null,
                },
            },
            Bike: {
                screen: BikeReservation,
                navigationOptions: {
                    title: 'Bike Reservation',
                },
            },
            BikeReservation: {
                screen: ReservationView,
                navigationOptions: {
                    title: 'Bike Reservation',
                },
            }
        }, {
            initialRouteName: 'List',
        }));
    }
    render() {
        const Screens = this.getStackNavigation();
        return (
            <View style={{ flex: 1, padding: 10}}>
                <Screens />
            </View>
        )
    }
}