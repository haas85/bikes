import React, { Component } from 'react';
import moment from 'moment';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Alert,
    StyleSheet
} from 'react-native';

import {deleteRental} from '../../actions/rentals'

import Average from '../Average';

import styles from '../../lib/styles';
import { Marker } from "react-native-maps"
import Map from '../Map'

const ReserveStyles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column'
    },
    info: {
        flexDirection: 'row'
    },
    attr: {
        fontWeight: 'bold'
    },
    pickerContainer: {
        marginTop: 20,
        flexDirection: 'column'
    },
    average: {
        width: '100%',
        alignItems: 'center'
    },
    separator: {
        borderBottomColor: 'grey',
        borderBottomWidth: 2
    }
});

class ReservationView extends Component {
    cancel = () => {
        const {navigation, cancelRental} = this.props;
        const rental = navigation.getParam('rental');
        cancelRental(rental.id)
            .then(() => {
                Alert.alert(
                    'Cancelled!',
                    'Your reservation has been cancelled',
                    [
                        {text: 'OK', onPress: () => navigation.goBack()},
                    ],
                    {cancelable: false},
                );
            })
            .catch(() => {
                Alert.alert(
                    'Error!',
                    'Error cancelling',
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            });
    }
    render() {
        const {navigation} = this.props;
        const rental = navigation.getParam('rental');
        const {bike} = rental

        const initial = moment(rental.initial);
        const end = moment(rental.end);
        const today = moment();

        return (
            <View style={styles.flex1}>
                <ScrollView
                    style={ReserveStyles.root}
                >
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>Model: </Text>
                        <Text>{bike.model}</Text>
                    </View>
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>Color: </Text>
                        <Text>{bike.color}</Text>
                    </View>
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>Weight: </Text>
                        <Text>{bike.weight}</Text>
                    </View>
                    <Map
                        style={{width: '100%', height: 200}}
                        region={bike.location || {}}
                    >{!!bike.location ?
                        <Marker
                            coordinate={bike.location}
                        />
                        : undefined}</Map>
                    <View style={{...ReserveStyles.average, ...styles.marginBottom}}>
                        <Average
                            average={bike.average}
                            bike={initial <= today && bike}
                            rental={initial <= today && rental}
                        />
                    </View>
                    <View style={{...styles.marginTop, ...styles.marginBottom, ...ReserveStyles.separator}}/>
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>From: </Text>
                        <Text>{initial.format('YYYY-MM-DD')}</Text>
                    </View>
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>To: </Text>
                        <Text>{end.format('YYYY-MM-DD')}</Text>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    style={{...styles.button, ...styles.deleteButton}}
                    onPress={this.cancel}
                >
                    <Text style={{color: 'white'}}>Cancel</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({
        cancelRental: deleteRental
    }, dispatch);

export default connect(null, mapDispatchToProps)(ReservationView);
