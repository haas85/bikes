import React from 'react';

import {
    createBottomTabNavigator
} from 'react-navigation'

import BikeListContainer from '../../containers/User/BikeListContainer';
import ReservationListContainer from '../../containers/User/ReservationListContainer';

export default () => {
    return createBottomTabNavigator({
        Bikes: BikeListContainer,
        Reservations:  ReservationListContainer,
    }, {
        initialRouteName: 'Bikes'
    });
}