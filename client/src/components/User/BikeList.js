import React, { Component } from 'react';
import moment from 'moment';
import {
    ScrollView,
    View,
    TouchableOpacity,
    Text,
    StyleSheet, TextInput
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Marker } from 'react-native-maps';

import Map from '../Map';

import BikeListElem from '../BikeListElem';
import styles from '../../lib/styles';

const ViewStyles = StyleSheet.create({
    messageView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        textAlign: 'center'
    },
    messageText: {
        textAlign: 'center',
        width: '100%',
        fontSize: 20
    },
    error: {
        color: 'red'
    },
    pickerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    picker: {
        width: 120
    },
    select: {
        height: 100,
        width: '100%',
        backgroundColor: 'red'
    },
    separator: {
        width: '100%',
        borderBottomColor: 'grey',
        borderBottomWidth: 2,
    },
    filtersButton: {
        width: '100%',
        textAlign: 'center',
        height: 20
    },
    filtersText: {
        width: '100%',
        textAlign: 'center',
        color: 'grey'
    },
    map: {
        width: '100%',
        height: '100%'
    }
});

export default class BikeList extends Component {
    state = {
        model: '',
        color: '',
        weight: undefined,
        rating: undefined,
    }
    getBikes = () => {
        const {initial, end} = this.state;
        const {getAvailableBikes} = this.props;
        if (!!initial && !!end && initial <= end) {
            getAvailableBikes(initial.toDate().getTime(), end.toDate().getTime());
        }
    }

    onBike = (bike) => {
        const {navigation} = this.props;
        const {initial, end} = this.state;
        navigation.navigate('Bike', {
            bike,
            range: {
                initial,
                end
            }
        });
    }

    getFilteredBikes = () => {
        const {model, color, weight, average} = this.state;
        const {bikes} = this.props;
        const _bikes = Object.values(bikes);
        return _bikes.filter((bike) => {
            let valid = true;
            if (model && model !== '' && !bike.model.toLowerCase().includes(model)) {
                valid = false;
            }
            if (color && color !== '' && !bike.color.toLowerCase().includes(color)) {
                valid = false;
            }
            if (weight && weight !== '' && bike.weight !== weight) {
                valid = false;
            }
            if (average && average !== '' && bike.average !== average) {
                valid = false;
            }
            return valid;
        })
    }

    getView = () => {
        const {initial, end, viewType, coords = {}} = this.state;
        if (!initial || !end) {
            return (<View style={ViewStyles.messageView}>
                <Text style={ViewStyles.messageText}>
                    Please select a date range
                </Text>
            </View>);
        } else if (initial > end ) {
            return (<View style={ViewStyles.messageView}>
                <Text style={{...ViewStyles.messageText, ...ViewStyles.error}}>
                    Please select a valid date range
                </Text>
            </View>);
        } else if(viewType === 'map') {
            const bikes = this.getFilteredBikes();
            if (!coords.latitude) {
                navigator.geolocation.getCurrentPosition(({coords}) => this.setState({coords}));
            }
            return (<View style={styles.flex1}>
                <View style={styles.flex1}>
                    <Map
                        style={ViewStyles.map}
                        region={coords}
                    >
                        {bikes.map((bike, i) =>
                            <Marker
                                key={`${bike.model}-${i}`}
                                coordinate={bike.location}
                                onPress={() => this.onBike(bike)}
                            />
                        )}
                    </Map>
                </View>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.setState({viewType: 'list'})}
                >
                    <Text>
                        Show List
                    </Text>
                </TouchableOpacity>
            </View>);
        } else {
            const bikes = this.getFilteredBikes();
            return (<View style={styles.flex1}>
                <ScrollView
                    style={styles.flex1}
                >
                    {bikes.map((bike) => <BikeListElem key={bike.id} bike={bike} onBike={this.onBike}/>)}
                </ScrollView>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.setState({viewType: 'map'})}
                >
                    <Text>
                        Show Map
                    </Text>
                </TouchableOpacity>
            </View>);
        }
    }

    getDatePickers = () => {
        const {initial, end} = this.state;
        return (
        <View style={ViewStyles.pickerContainer}>
            <View style={{...styles.marginBottom}}>
                <Text>Select start date:</Text>
                <View>
                    <DatePicker
                        style={ViewStyles.picker}
                        date={!!initial ? initial.format('YYYY-MM-DD') : undefined}
                        mode="date"
                        placeholder="Select start date"
                        format="YYYY-MM-DD"
                        minDate={moment().format('YYYY-MM-DD')}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                display: 'none'
                            }
                        }}
                        onDateChange={(date) => {this.setState({initial: moment(date)})}}
                    />
                </View>
            </View>
            <View style={{...styles.marginBottom}}>
                <Text>Select end date:</Text>
                <DatePicker
                    style={ViewStyles.picker}
                    date={!!end ? end.format('YYYY-MM-DD') : undefined}
                    mode="date"
                    placeholder="Select end date"
                    format="YYYY-MM-DD"
                    minDate={moment().format('YYYY-MM-DD')}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            display: 'none'
                        }
                    }}
                    onDateChange={(date) => {this.setState({end: moment(date)})}}
                />
            </View>
        </View>
    )}

    getFilters = () => {
        const {model, color, weight, average, showFilters = false} = this.state;
        const {bikes} = this.props;
        const _bikes = Object.values(bikes);
        if (_bikes.length === 0) {
            return (<View />);
        }
        if (showFilters) {
            return (<View>
                <View style={ViewStyles.separator}></View>
                <TextInput
                    placeholder='Model'
                    style={{...styles.input, ...styles.marginBottom}}
                    onChangeText={(text) => this.setState({model: text.toLowerCase()})}
                    value={model}
                />
                <TextInput
                    placeholder='Color'
                    style={{...styles.input, ...styles.marginBottom}}
                    onChangeText={(text) => this.setState({color: text.toLowerCase()})}
                    value={color}
                />
                <TextInput
                    placeholder='Weight'
                    style={{...styles.input, ...styles.marginBottom}}
                    keyboardType='numeric'
                    onChangeText={(text) => this.setState({weight: text !== '' ? parseInt(text, 10) : undefined})}
                    value={weight !== undefined ? `${weight}` : weight}
                />
                <TextInput
                    placeholder='Rating'
                    style={{...styles.input, ...styles.marginBottom}}
                    keyboardType='numeric'
                    onChangeText={(text) => this.setState({average: text !== '' ? parseInt(text, 10) : undefined})}
                    value={average !== undefined ? `${average}` : average}
                />
                <TouchableOpacity
                    style={ViewStyles.filtersButton}
                    onPress={() => this.setState({showFilters: false})}
                >
                    <Text
                        style={ViewStyles.filtersText}
                    >
                        Hide Filters
                    </Text>
                </TouchableOpacity>
            </View>);
        }

        return (<TouchableOpacity
            style={ViewStyles.filtersButton}
            onPress={() => this.setState({showFilters: true})}
        >
            <Text
                style={ViewStyles.filtersText}
            >
                Filters
            </Text>
        </TouchableOpacity>);
    }

    render() {
        const {initial, end} = this.state;
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View>
                    {this.getDatePickers()}
                    {this.getFilters()}
                    <TouchableOpacity
                        style={{
                            ...styles.button,
                            ...(!!initial && !!end && initial <= end ? {} : styles.disabled)
                        }}
                        onPress={() => this.getBikes()}>
                        <Text>Search</Text>
                    </TouchableOpacity>
                </View>
                {this.getView()}
            </View>
        );
    }
}