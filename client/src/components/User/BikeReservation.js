import React, { Component } from 'react';
import moment from 'moment';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Alert,
    StyleSheet
} from 'react-native';

import DatePicker from 'react-native-datepicker';

import {createRental} from '../../actions/rentals'

import Average from '../Average';

import styles from '../../lib/styles';
import { Marker } from "react-native-maps"
import Map from '../Map'

const ReserveStyles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column'
    },
    info: {
        flexDirection: 'row'
    },
    attr: {
        fontWeight: 'bold'
    },
    pickerContainer: {
        marginTop: 20,
        flexDirection: 'column'
    },
    average: {
        width: '100%',
        alignItems: 'center'
    },
    separator: {
        borderBottomColor: 'grey',
        borderBottomWidth: 2
    },
    error: {
        width: '100%',
        textAlign: 'center',
        color: 'red',
        fontSize: 16,
        marginTop: 20,
        marginBottom: 20
    }
});

class BikeReservation extends Component {
    state = {}

    rent = () => {
        const {navigation, rent} = this.props;
        const bike = navigation.getParam('bike').id;
        const {initial = moment(), end = moment().add(1, 'day')} = navigation.getParam('range') || {};
        const start = moment(this.state.initial || initial).toDate().getTime();
        const finish = moment(this.state.end || end).toDate().getTime();

        rent(start, finish, bike)
            .then(() => {
                Alert.alert(
                    'Reserved!',
                    'Bike reserved. See you soon!!!',
                    [
                        {text: 'OK', onPress: () => this.props.navigation.goBack()},
                    ],
                    {cancelable: false},
                );
            })
            .catch((err) => {
                Alert.alert(
                    err.status === 409 ? 'Bike already reserved' : 'Error!',
                    err.status === 409 ? 'Cannot book bike for that date, try a different date or bike'
                        : 'Error booking bike, try a different date',
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            });
    }

    render() {
        const {navigation} = this.props;
        const bike = navigation.getParam('bike');
        const {initial = moment(), end = moment().add(1, 'day')} = navigation.getParam('range') || {};
        const dates = {
            initial: this.state.initial || initial,
            end: this.state.end || end
        };
        let disabled;
        let error;
        if (dates.initial > dates.end) {
            disabled = true;
            error = 'Start date must me lower than the end one'
        }
        return (
            <View style={styles.flex1}>
                <ScrollView
                    style={ReserveStyles.root}
                >
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>Model: </Text>
                        <Text>{bike.model}</Text>
                    </View>
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>Color: </Text>
                        <Text>{bike.color}</Text>
                    </View>
                    <View style={{...ReserveStyles.info, ...styles.marginBottom}}>
                        <Text style={ReserveStyles.attr}>Weight: </Text>
                        <Text>{bike.weight}</Text>
                    </View>
                    <Map
                        style={{width: '100%', height: 200}}
                        region={bike.location || {}}
                    >{!!bike.location ?
                        <Marker
                            coordinate={bike.location}
                        />
                        : undefined}</Map>
                    <View style={{...ReserveStyles.average, ...styles.marginBottom}}>
                        <Average average={bike.average} />
                    </View>
                    <View style={{...styles.marginTop, ...styles.marginBottom, ...ReserveStyles.separator}}/>
                    <View style={{...styles.marginBottom, ...ReserveStyles.pickerContainer}}>
                        <Text>Select start date:</Text>
                        <View>
                            <DatePicker
                                style={{width: 200}}
                                date={dates.initial.format('YYYY-MM-DD')}
                                mode="date"
                                placeholder="Select start date"
                                format="YYYY-MM-DD"
                                minDate={moment().format('YYYY-MM-DD')}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36
                                    }
                                }}
                                onDateChange={(date) => {this.setState({initial: moment(date)})}}
                            />
                        </View>
                    </View>
                    <View style={{...styles.marginBottom, ...ReserveStyles.pickerContainer}}>
                        <Text>Select end date:</Text>
                        <DatePicker
                            style={{width: 200}}
                            date={dates.end.format('YYYY-MM-DD')}
                            mode="date"
                            placeholder="Select end date"
                            format="YYYY-MM-DD"
                            minDate={moment().format('YYYY-MM-DD')}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => {this.setState({end: moment(date)})}}
                        />
                    </View>
                </ScrollView>
                {!!error && <Text style={ReserveStyles.error}>{error}</Text>}
                <TouchableOpacity
                    style={{...styles.button, ...(disabled ? styles.disabled : {})}}
                    disabled={disabled}
                    onPress={this.rent}
                >
                    <Text>Reserve</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({
        rent: createRental
    }, dispatch);

export default connect(null, mapDispatchToProps)(BikeReservation);
