import React, { Component } from 'react';
import {
    TouchableOpacity,
    View, StyleSheet, Alert,
} from 'react-native'


import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Icon from 'react-native-vector-icons/FontAwesome';

import {rateBike} from '../actions/bike';

const BikeStyles = StyleSheet.create({
    root: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        flexDirection: 'row'
    }
});

class Average extends Component {
    state = {}
    rate = (value) => {
        const {bike, rental, rate} = this.props;
        if (bike && rental) {
            rate(bike.id, rental.id, value)
                .then((data) => {
                    Alert.alert(
                        'Rated!',
                        `Bike rated with ${value} points`,
                        [
                            {text: 'OK', onPress: () => this.setState({average: data.value.average})},
                        ],
                        {cancelable: false},
                    );
                })
                .catch(() => {
                    Alert.alert(
                        'Error!',
                        'Error rating bike',
                        [
                            {text: 'OK'},
                        ],
                        {cancelable: false},
                    );
                });
        }
    }

    render() {
        const {average} = this.props;
        const value = this.state.average || average;
        return (
            <View style={BikeStyles.root}>
                <View style={BikeStyles.container}>
                    {[1,2,3,4,5].map((index) =>
                        <TouchableOpacity
                            key={index}
                            onPress={() => this.rate(index)}
                        >
                            <Icon name="star" size={20} color={index <= value ? '#efdd3b' : 'black'} />
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({
        rate: rateBike
    }, dispatch);

export default connect(null, mapDispatchToProps)(Average);