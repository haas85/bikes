import React, { Component } from 'react';
import moment from 'moment';
import {
    Text,
    TouchableOpacity,
    Image,
    View, StyleSheet,
} from 'react-native';

import Average from './Average';

import styles from '../lib/styles';

const BikeStyles = StyleSheet.create({
    image: {
        marginRight: 5
    },
    root: {
        width: '100%',
        flexDirection: 'row',
        marginBottom: 5
    },
    info: {
        flex: 1,
        flexDirection: 'row'
    },
    name: {
        width: '50%'
    },
    average: {
        width: '50%'
    }
});

export default class BikeRentalElem extends Component {
    state = {}

    componentDidMount() {
        const {photo} = this.props.rental.bike;
        Image.getSize(photo, (width, height) => {
            this.setState({
                height: 40,
                width: width / (height / 40)
            })
        });
    }

    render() {
        const {rental, onRental = () => {}} = this.props;
        const {model, photo, color, average} = rental.bike;
        const {height, width} = this.state;
        return (
            <TouchableOpacity
                style={BikeStyles.root}
                onPress={() => onRental(rental)}
            >
                {height && <Image
                    style={{...BikeStyles.image, height, width}}
                    source={{uri: photo}}
                />}
                <View style={ BikeStyles.info}>
                    <View style={BikeStyles.name}>
                        <Text>{model}</Text>
                        <Text>{color}</Text>
                        <Text>From: {moment(rental.initial).format('MM/DD/YYYY')}</Text>
                        <Text>To: {moment(rental.end).format('MM/DD/YYYY')}</Text>
                    </View>
                    <View style={BikeStyles.average}>
                        <Average average={average} />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}