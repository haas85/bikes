import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    View, StyleSheet,
} from 'react-native';

import styles from '../lib/styles';

const UserStyles = StyleSheet.create({
    root: {
        width: '100%',
        flexDirection: 'column',
        padding: 5
    },
    info: {
        width: '100%',
        flexDirection: 'column'
    }
});

export default class UserListElem extends Component {
    render() {
        const {user, onUser = () => {}} = this.props;
        const {mail, role} = user;
        return (
            <TouchableOpacity
                style={UserStyles.root}
                onPress={() => onUser(user)}
            >
                <View style={UserStyles.info}>
                    <Text style={styles.marginBottom}>{mail}</Text>
                    <Text>{role}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}