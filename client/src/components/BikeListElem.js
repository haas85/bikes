import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    Image,
    View, StyleSheet,
} from 'react-native';

import Average from './Average';

import styles from '../lib/styles';

const BikeStyles = StyleSheet.create({
    image: {
        marginRight: 5
    },
    root: {
        width: '100%',
        flexDirection: 'row',
        marginBottom: 5
    },
    info: {
        flex: 1,
        flexDirection: 'row'
    },
    name: {
        width: '50%'
    },
    average: {
        width: '50%'
    }
});

export default class BikeListElem extends Component {
    state = {}

    componentDidMount() {
        const {photo} = this.props.bike;
        Image.getSize(photo, (width, height) => {
            this.setState({
                height: 40,
                width: width / (height / 40)
            })
        });
    }

    render() {
        const {bike, onBike = () => {}} = this.props;
        const {model, photo, color, average} = bike;
        const {height, width} = this.state;
        return (
            <TouchableOpacity
                style={BikeStyles.root}
                onPress={() => onBike(bike)}
            >
                {height && <Image
                    style={{...BikeStyles.image, height, width}}
                    source={{uri: photo}}
                />}
                <View style={ BikeStyles.info}>
                    <View style={BikeStyles.name}>
                        <Text>{model}</Text>
                        <Text>{color}</Text>
                    </View>
                    <View style={BikeStyles.average}>
                        <Average average={average} />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}