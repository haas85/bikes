import React from 'react';
import MapView from 'react-native-maps';

export default (
    {
        style = {width: '100%', height: 300},
        onPress = () => {},
        region = {},
        children
    }) => (
    <MapView
        style={style}
        onPress={(data) => {onPress(data.nativeEvent.coordinate);}}
        initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            ...region,
            latitudeDelta: 0.02,
            longitudeDelta: 0.021,
        }}
    >{children}</MapView>
);