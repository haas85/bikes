import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import CredentialsContainer from './CredentialsContainer';
import AppContainer from './AppContainer';

class MainContainer extends Component {
    render() {
        const {user} = this.props;
        return (<View style={{flex: 1, width: '100%'}}>
            {
                !user.mail ? <CredentialsContainer /> : <AppContainer />
            }
        </View>)
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);