import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, AsyncStorage} from 'react-native';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {logout}  from '../actions/user';

import Manager from '../components/Manager';
import User from '../components/User';

import commonStyles from '../lib/styles'

const styles = StyleSheet.create({
    root: {
        flex: 1,
        width: '100%'
    },
    header: {
        height: 50,
        width: '100%',
        backgroundColor: '#8eb9ff',
        textAlign: 'right'
    },
    body: {
        flex: 1,
        width: '100%'
    }
});

class AppContainer extends Component {
    render() {
        return (
            <View style={styles.root}>
                <View style={styles.header}>
                    <View />
                    <TouchableOpacity
                        style={{...commonStyles.button, width: 100, height: '100%', alignSelf: 'flex-end'}}
                        onPress={() => this.props.logout().finally(() => AsyncStorage.removeItem('@BIKE:USER'))}
                    >
                        <Text>Logout</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.body}>
                    {this.props.user.role === 'MANAGER' ? <Manager /> : <User />}
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        logout
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);