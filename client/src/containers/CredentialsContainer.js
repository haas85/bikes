import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {login, signup}  from '../actions/user';

import Login from '../components/credentials/Login';
import Signup from '../components/credentials/Signup';

class CredentialsContainer extends Component {
    getScreens = () => {
        return createStackNavigator({
            Login: {
                screen: (props) =>
                    <Login {...props} onLogin={(mail, password) => this.props.login(mail, password)} />,
                navigationOptions: {
                    title: "Log In"
                }
            },
            Signup: {
                screen: (props) =>
                    <Signup {...props} onSignup={(mail, password) => this.props.signup(mail, password)} />,
                navigationOptions: {
                    title: "Sign Up"
                }
            }
        },{
            initialRouteName: 'Login'
        });
    }

    render() {
        const Screens = createAppContainer(this.getScreens());
        return (<View style={{flex: 1, width: '100%'}}>
            <Screens />
        </View>)
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        login,
        signup
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(CredentialsContainer);