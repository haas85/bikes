import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getAvailableBikes}  from '../../actions/bike';

import BikeList from '../../components/User/BikeList';

const BikeListContainer = (props) =>
    <BikeList {...props}/>

const mapStateToProps = (state) => ({
    bikes: state.bikes
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        getAvailableBikes
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(BikeListContainer);