import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getRentals}  from '../../actions/rentals';

import UserRentalList from '../../components/UserRentalList';

const BikeListContainer = (props) =>
    <UserRentalList
        {...props}
        onRental={(rental) => props.navigation.navigate('BikeReservation', {rental})}
    />

const mapStateToProps = (state) => ({
    rentals: state.rentals
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        getRentals
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(BikeListContainer);