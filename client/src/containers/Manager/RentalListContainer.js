import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {createRental, deleteRental, getRental, getRentals}  from '../../actions/rentals';

import UserRentalList from '../../components/UserRentalList';
import BikeRentalList from '../../components/Manager/BikeRentalList';

const RentalListContainer = (props) =>
    (props.navigation.getParam('bike') || {}).id ?  <BikeRentalList {...props}/> : <UserRentalList {...props}/>

const mapStateToProps = (state) => ({
    rentals: state.rentals
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        createRental,
        deleteRental,
        getRental,
        getRentals
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(RentalListContainer);