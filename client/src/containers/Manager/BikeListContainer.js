import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {createBike, deleteBike, editBike, getBike, getBikes}  from '../../actions/bike';

import BikeList from '../../components/Manager/BikeList';

const BikeListContainer = (props) =>
    <BikeList {...props}/>

const mapStateToProps = (state) => ({
    bikes: state.bikes
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        createBike,
        deleteBike,
        editBike,
        getBike,
        getBikes
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(BikeListContainer);