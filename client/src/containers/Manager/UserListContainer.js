import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {createUser, deleteUser, editUser, getUser, getUsers}  from '../../actions/user';

import UserList from '../../components/Manager/UserList';

const UserListContainer = (props) =>
    <UserList {...props}/>

const mapStateToProps = (state) => ({
    users: state.users
});

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        createUser,
        deleteUser,
        editUser,
        getUser,
        getUsers
    }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(UserListContainer);