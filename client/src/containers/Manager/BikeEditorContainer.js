import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ImagePicker from 'react-native-image-picker';
import { Marker } from 'react-native-maps';

import {View, Text, TextInput, TouchableOpacity, ScrollView, Alert, Image} from 'react-native'

import {createBike, deleteBike, editBike} from '../../actions/bike';
import FullWidthImage from '../../components/FullWidthImage';

import styles from '../../lib/styles';
import Map from '../../components/Map'

class BikeEditorContainer extends Component {
    emptyBike = {
        location: undefined,
        model: '',
        color: '',
        weight: '',
        photo: '',
        available: true,
        coords: undefined
    }

    state = {}

    selectPicture = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    photo: response,
                });
            }
        });
    }

    save = () => {
        const {navigation} = this.props;
        const bike = navigation.getParam('bike') || this.emptyBike;
        const values = {};
        Object.keys(this.emptyBike).forEach((key) => {
            values[key] = this.state.hasOwnProperty(key) ? this.state[key] : bike[key];
            if (values[key] === '') {
            }
        });
        if (navigation.getParam('bike')) {
            values.id = navigation.getParam('bike').id;
            this.props.editBike(values).then(() => {
                Alert.alert(
                    'Edited',
                    'Bike edited properly',
                    [
                        {text: 'OK', onPress: () => navigation.navigate('List')},
                    ],
                    {cancelable: false},
                );
            }).catch(() => {
                Alert.alert(
                    'Error Editing',
                    'Error editing bike',
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            });
        } else {
            this.props.createBike(values).then(() => {
                Alert.alert(
                    'Created',
                    'Bike created properly',
                    [
                        {text: 'OK', onPress: () => navigation.goBack()},
                    ],
                    {cancelable: false},
                );
            }).catch(() => {
                Alert.alert(
                    'Error Creating',
                    'Error creating bike',
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            });
        }
    }

    delete = () => {
        const {navigation} = this.props;
        const bike = navigation.getParam('bike');
        this.props.deleteBike(bike.id).then(() => {
            Alert.alert(
                'Deleted',
                'Bike deleted properly',
                [
                    {text: 'OK', onPress: () => navigation.navigate('List')},
                ],
                {cancelable: false},
            );
        }).catch(() => {
            Alert.alert(
                'Error Deleting',
                'Error deleting bike',
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
        });
    }

    render() {
        const {navigation} = this.props;
        const bike = navigation.getParam('bike') || this.emptyBike;
        const {coords} = this.state;
        const values = {};
        let disabled = false;
        Object.keys(this.emptyBike).forEach((key) => {
            values[key] = this.state.hasOwnProperty(key) ? this.state[key] : bike[key];
            if (values[key] === '') {
                disabled = true;
            }
        });
        if (!coords) {
            navigator.geolocation.getCurrentPosition(({coords}) => this.setState({coords}));
        }

        return (
            <View style={{...styles.container}}>
                <ScrollView style={{flex: 1}}>
                    <TextInput
                        placeholder='Model'
                        style={{...styles.input, ...styles.marginBottom}}
                        onChangeText={(text) => this.setState({model: text})}
                        value={values.model}
                    />
                    <TextInput
                        placeholder='Color'
                        style={{...styles.input, ...styles.marginBottom}}
                        onChangeText={(text) => this.setState({color: text})}
                        value={values.color}
                    />
                    <TextInput
                        placeholder='Weight'
                        style={{...styles.input, ...styles.marginBottom}}
                        keyboardType='numeric'
                        onChangeText={(text) => this.setState({weight: text})}
                        value={`${values.weight}`}
                    />
                    <Map
                        style={{width: '100%', height: 200}}
                        region={coords || values.location || {}}
                        onPress={(location) => this.setState({location: JSON.parse(JSON.stringify(location))})}
                    >{!!values.location ?
                        <Marker
                            coordinate={values.location}
                        />
                        : undefined}</Map>
                    <View>
                        <TouchableOpacity
                            style={{...styles[values.available ? 'button' : 'secondary'],...styles.marginBottom}}
                            onPress={() => this.setState({available: !values.available})}
                        ><Text>{values.available ? 'Available' : 'Unavailable'}</Text></TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity
                            style={{...styles.button,...styles.marginBottom}}
                            onPress={this.selectPicture}
                        ><Text>Select picture</Text></TouchableOpacity>
                        {!!values.photo && values.photo !== '' &&
                        <FullWidthImage
                            source={{uri: values.photo.uri || values.photo}}
                        />}
                    </View>
                </ScrollView>
                <View>
                    <TouchableOpacity
                        style={{...styles.button,...styles.marginBottom,  ...(disabled ? styles.disabled : {})}}
                        onPress={this.save}
                        disabled={disabled}
                    ><Text>Save</Text></TouchableOpacity>
                    {bike.hasOwnProperty('id') && <TouchableOpacity
                        style={{...styles.button, ...styles.deleteButton}}
                        onPress={this.delete}
                        disabled={disabled}
                    ><Text style={{...styles.deleteButton}}>Delete</Text></TouchableOpacity>}
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        createBike,
        deleteBike,
        editBike
    }, dispatch)
)

export default connect(null, mapDispatchToProps)(BikeEditorContainer);