import React, {Component} from 'react';
import validator from 'validator';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {View, Text, TextInput, TouchableOpacity, ScrollView, Alert} from 'react-native'

import {createUser, deleteUser, editUser}  from '../../actions/user';

import styles from '../../lib/styles';

class UserEditorContainer extends Component {
    emptyUser = {
        mail: '',
        role: 'USER'
    }

    state = {}

    save = () => {
        const {navigation} = this.props;
        const user = navigation.getParam('user') || this.emptyUser;
        const values = {};
        Object.keys(this.emptyUser).forEach((key) => {
            values[key] = this.state[key] || user[key];
            if (values[key] === '') {
            }
        });
        if (navigation.getParam('user')) {
            values.id = navigation.getParam('user').id;
            this.props.editUser(values).then(() => {
                Alert.alert(
                    'Edited',
                    'User edited properly',
                    [
                        {text: 'OK', onPress: () => navigation.navigate('List')},
                    ],
                    {cancelable: false},
                );
            }).catch(() => {
                Alert.alert(
                    'Error Editing',
                    'Error editing user',
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            });
        } else {
            this.props.createUser({...values, password: this.state.password}).then(() => {
                Alert.alert(
                    'Created',
                    'User created properly',
                    [
                        {text: 'OK', onPress: () => navigation.goBack()},
                    ],
                    {cancelable: false},
                );
            }).catch(() => {
                Alert.alert(
                    'Error Creating',
                    'Error creating user',
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            });
        }
    }

    delete = () => {
        const {navigation} = this.props;
        const user = navigation.getParam('user');
        this.props.deleteUser(user.id).then(() => {
            Alert.alert(
                'Deleted',
                'User deleted properly',
                [
                    {text: 'OK', onPress: () => navigation.navigate('List')},
                ],
                {cancelable: false},
            );
        }).catch(() => {
            Alert.alert(
                'Error Deleting',
                'Error deleting user',
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
        });
    }

    getError = () => {
        const {navigation} = this.props;
        const {mail, password} = this.state;
        if (mail && mail !== '' && !validator.isEmail(mail)) {
            return 'Please insert a valid mail format';
        }

        if (navigation.getParam('user') && password && password !== '' && password.length < 6) {
            return 'Passwords too short';
        }

        return false;
    }

    render() {
        const {navigation} = this.props;
        const user = navigation.getParam('user') || this.emptyUser;
        const values = {};
        let disabled = false;
        const creating = !user.hasOwnProperty('id');
        Object.keys(this.emptyUser).forEach((key) => {
            values[key] = this.state[key] || user[key];
            if (values[key] === '') {
                disabled = true;
            }
        });
        let password;
        if (creating) {
            password = this.state.password || '';
            if (password === '') {
                disabled = true;
            }
        }
        const error = this.getError();

        if (error) {
            disabled = true;
        }

        return (
            <View style={{...styles.container}}>
                <ScrollView style={{flex: 1}}>
                    <TextInput
                        placeholder='Mail'
                        textContentType="emailAddress"
                        style={{...styles.input, ...styles.marginBottom}}
                        onChangeText={(text) => this.setState({mail: text.toLowerCase()})}
                        value={values.mail}
                    />
                    {creating &&
                    <TextInput
                        placeholder='Password'
                        textContentType="password"
                        secureTextEntry={true}
                        style={{...styles.input, ...styles.marginBottom}}
                        onChangeText={(text) => this.setState({password: text})}
                        value={password}
                    />}
                    <View style={styles.marginBottom}>
                        <Text style={styles.marginBottom}>Role</Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                            <TouchableOpacity
                                style={values.role === 'USER' ? styles.button : styles.secondary}
                                onPress={() => this.setState({role: 'USER'})}
                            ><Text>User</Text></TouchableOpacity>
                            <TouchableOpacity
                                style={values.role === 'MANAGER' ? styles.button : styles.secondary}
                                onPress={() => this.setState({role: 'MANAGER'})}
                            ><Text>Manager</Text></TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <View>
                    {error &&
                    <View style={{margin:7}} >
                        <Text style={{color: 'red'}}>{error}</Text>
                    </View>
                    }
                    <TouchableOpacity
                        style={{...styles.button,...styles.marginBottom,  ...(disabled ? styles.disabled : {})}}
                        onPress={this.save}
                        disabled={disabled}
                    ><Text>Save</Text></TouchableOpacity>
                    {!creating && <TouchableOpacity
                        style={{...styles.button, ...styles.deleteButton}}
                        onPress={this.delete}
                        disabled={disabled}
                    ><Text style={{...styles.deleteButton}}>Delete</Text></TouchableOpacity>}
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        createUser,
        deleteUser,
        editUser
    }, dispatch)
)

export default connect(null, mapDispatchToProps)(UserEditorContainer);