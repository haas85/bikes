import {
    createRental as createRentalApi,
    getRentals as getRentalsApi,
    deleteRental as deleteRentalApi
} from '../lib/api';

export const ACTIONS = {
    GET_LIST: 'RENTAL_GET_LIST',
    CREATE: 'RENTAL_CREATE',
    DELETE: 'RENTAL_DELETE',
    GET: 'RENTAL_GET',
};

export const getRentals = (filter) => ({
    type: ACTIONS.GET_LIST,
    payload: getRentalsApi(filter)
});

export const createRental = (initial, end, bike) => ({
    type: ACTIONS.CREATE,
    payload: createRentalApi(initial, end, bike)
});

export const deleteRental = (id) => ({
    type: ACTIONS.DELETE,
    payload: deleteRentalApi(id),
    meta: {
        id
    }
});

export const getRental = (id) => ({
    type: ACTIONS.DELETE,
    payload: new Promise((resolve) => {
        resolve({
            id: id,
            initial: new Date().getTime(),
            end: new Date().getTime(),
            bike: {
                id: id,
                name: `Bike ${id}`,
                location: `location ${id}`,
                model: `model ${id}`,
                color: `color ${id}`,
                weight: id,
                photo: 'https://www.wigglestatic.com/product-media/100375136/Brand-X-Road-Bike-Road-Bikes-Black-2017-BRNDXROADXL-0.jpg?w=960&h=430&a=7',
                average: 3
            },
            user: {
                id: id,
                mail: `mail${id}@gmail.com`,
                role: 'USER',
            }
        });
    })
});