import {
    getUser as getUserApi,
    getUsers as getUsersApi,
    login as loginApi,
    signup as signupApi,
    logout as logoutApi,
    createUser as createUserApi,
    editUser as editUserApi,
    deleteUser as deleteUserApi
} from '../lib/api';

import {AsyncStorage} from 'react-native'

export const ACTIONS = {
    LOAD: 'USER_LOAD',
    LOGIN: 'USER_LOGIN',
    LOGOUT: 'USER_LOGOUT',
    GET_CURRENT: 'USER_GET_CURRENT',
    GET_LIST: 'USER_GET_LIST',
    CREATE: 'USER_CREATE',
    EDIT: 'USER_EDIT',
    DELETE: 'USER_DELETE',
    GET: 'USER_GET',
};

export const load = () => ({
    type: ACTIONS.LOAD,
    payload: AsyncStorage.getItem('@BIKE:USER')
});

export const login = (mail, password) => ({
    type: ACTIONS.LOGIN,
    payload: loginApi(mail, password)
});

export const signup = (mail, password) => ({
    type: ACTIONS.LOGIN,
    payload: signupApi(mail, password)
});

export const logout = () => ({
    type: ACTIONS.LOGOUT,
    payload: logoutApi()
});

export const getCurrentUser = () => ({
    type: ACTIONS.GET_CURRENT,
    payload: getUserApi()
});

export const getUsers = () => ({
    type: ACTIONS.GET_LIST,
    payload: getUsersApi()
});

export const createUser = ({mail, password, role}) => ({
    type: ACTIONS.CREATE,
    payload: createUserApi(mail, password, role)
});

export const editUser = (user) => ({
    type: ACTIONS.EDIT,
    payload: editUserApi(user.id, user),
    meta: {
        user
    }
});

export const deleteUser = (id) => ({
    type: ACTIONS.DELETE,
    payload: deleteUserApi(id),
    meta: {
        id
    }
});

export const getUser = (id) => ({
    type: ACTIONS.DELETE,
    payload: new Promise((resolve) => {
        resolve({
            id: id,
            mail: `mail ${id}`,
            role: 'USER',
        });
    })
});