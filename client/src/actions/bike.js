import {
    getBike as getBikeApi,
    getBikes as getBikesApi,
    getAvailableBikes as getAvailableBikesApi,
    createBike as createBikeApi,
    editBike as editBikeApi,
    deleteBike as deleteBikeApi,
    rateBike as rateBikeApi
} from '../lib/api';


export const ACTIONS = {
    GET_LIST: 'BIKE_GET_LIST',
    GET_AVAILABLE: 'BIKE_GET_AVAILABLE',
    CREATE: 'BIKE_CREATE',
    EDIT: 'BIKE_EDIT',
    DELETE: 'BIKE_DELETE',
    GET: 'BIKE_GET',
    RATE: 'BIKE_RATE',
};

export const getBikes = () => ({
    type: ACTIONS.GET_LIST,
    payload: getBikesApi()
});

export const getAvailableBikes = (from, to) => ({
    type: ACTIONS.GET_AVAILABLE,
    payload: getAvailableBikesApi(from, to)
});

export const createBike = (bike) => ({
    type: ACTIONS.CREATE,
    payload: createBikeApi(bike)
});

export const editBike = (bike) => ({
    type: ACTIONS.EDIT,
    payload: editBikeApi(bike),
    meta: {
        bike
    }
});

export const deleteBike = (id) => ({
    type: ACTIONS.DELETE,
    payload: deleteBikeApi(id),
    meta: {
        id
    }
});

export const getBike = (id) => ({
    type: ACTIONS.DELETE,
    payload: getBikeApi(id)
});

export const rateBike = (bike, rental, rating) => ({
    type: ACTIONS.RATE,
    payload: rateBikeApi(bike, rental, rating)
});