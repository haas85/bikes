import { StyleSheet } from 'react-native';
const PRIMARY_COLOR = '#42cef4';
const SECONDARY_COLOR = '#DDDDDD';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    flex1: {
        flex: 1
    },
    button: {
        alignItems: 'center',
        backgroundColor: PRIMARY_COLOR,
        color: '#eff7f9',
        padding: 10
    },
    secondary: {
        alignItems: 'center',
        backgroundColor: SECONDARY_COLOR,
        padding: 10
    },
    deleteButton: {
        backgroundColor: '#ce0014',
        color: 'white'
    },
    countContainer: {
        alignItems: 'center',
        padding: 10
    },
    marginTop: {
        marginTop: 10
    },
    marginBottom: {
        marginBottom: 10
    },
    countText: {
        color: '#FF00FF'
    },
    input: {
        width: '100%',
        height: 40,
        borderBottomColor: SECONDARY_COLOR,
        borderBottomWidth: 2
    },
    disabled: {
        backgroundColor: '#f7f7f7',
        color: '#e0e0e0'
    }
});
