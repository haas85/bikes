export const SERVER_URL = 'http://localhost:3010';
export const API_URL = `${SERVER_URL}/api/1`;

const request = (url, extra = {}) => {
    const headers = extra.headers || {};
    if (window.user) {
        headers.Authorization = window.user.token;
    }
    return fetch(`${API_URL}${url}`, {...extra, headers})
        .then((response) => {
            if (window.cosa) {
                debugger;
            }
            if (!response.ok) {
                return new Promise((resolve, reject) => {
                    response.json().then((data) => {
                        reject({
                            status: response.status,
                            data
                        });
                    })
                });
            }
            return response.json()
        })
        .then((response) => {
            console.log(`%c ${API_URL}${url}`, 'color: green', response);
            return response;
        })
        .catch((error) => {
            console.log(`%c ${API_URL}${url}`, 'background: red; color: white', error);
            throw error;
        })
}

export const login = (mail, password) =>
    request('/user/login', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify({
            mail,
            password
        }),
    });

export const signup = (mail, password) =>
    request('/user/signup', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            mail,
            password
        }),
    });

export const logout = () =>
    request('/user/logout', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'include',
    });


export const getUser = (id) => {
    if(id) {
        return request(`/user/$id`);
    }
    return request('/user');
}

export const getUsers = () => {
    return request('/user/all');
}

export const createUser = (mail, password, role) =>
    request('/user/create', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            mail,
            password,
            role
        }),
    });

export const editUser = (id, data) =>
    request(`/user/${id}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });

export const deleteUser = (id,) =>
    request(`/user/${id}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }
    });

export const getBike = (id) => {
    return request(`/bike/${id}`);
}

export const getBikes = () => {
    return request('/bike');
}


export const getAvailableBikes = (from, to) => {
    return request(`/bike/available?fromDate=${from}&toDate=${to}`);
}

export const createBike = (bike) => {
    const _bike = {
        ...bike,
        location: JSON.stringify(bike.location)
    }
    let formdata = new FormData();
    const {photo, ...data} = _bike;
    Object.keys(data).forEach((key) => {
        formdata.append(key, data[key]);
    });
    formdata.append('photo', {uri: photo.uri, name: photo.fileName, type: photo.type});
    return request('/bike', {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
        },
        body: formdata,
    });
}

export const editBike = (bike) => {
    const _bike = {
        ...bike,
        location: JSON.stringify(bike.location)
    }
    let formdata = new FormData();
    const {id, photo, ...data} = _bike;
    Object.keys(data).forEach((key) => {
        formdata.append(key, data[key]);
    });
    if (photo && photo.uri) {
        formdata.append('photo', {uri: photo.uri, name: photo.fileName, type: photo.type});
    }
    return request(`/bike/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'multipart/form-data',
        },
        body: formdata,
    });
}

export const deleteBike = (id) =>
    request(`/bike/${id}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }
    });

export const rateBike = (bike, rental, rating) =>
    request(`/rating/${bike}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            rental,
            rating
        }),
    }).then(() => getBike(bike));

export const createRental = (initial, end, bike) =>
    request('/rental', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            initial,
            end,
            bike
        }),
    });

export const getRentals = (filter = {}) => {
    console.log(filter)
    if (Object.keys(filter).length !== 0) {
        return request(`/rental?${Object.keys(filter).map((key) => `${key}=${filter[key]}`).join('#')}`);
    }
    return request('/rental');
}

export const deleteRental = (id) =>
    request(`/rental/${id}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }
    });