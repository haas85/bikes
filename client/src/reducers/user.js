import {ACTIONS as USER_ACTIONS} from '../actions/user';

export default (state = {}, action) => {
    const {type, payload} = action;
    switch (type) {
        case `${USER_ACTIONS.LOAD}_FULFILLED`:
            window.user = JSON.parse(payload).value;
            return window.user;
        case `${USER_ACTIONS.LOGIN}_FULFILLED`: {
            window.user = payload;
            return {...payload}
        }
        case `${USER_ACTIONS.LOGOUT}_FULFILLED`: {
            delete window.user;
            return {};
        }
        default: return state;
    }
};