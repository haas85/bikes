import {SERVER_URL} from '../lib/api';

import {ACTIONS as BIKE_ACTIONS} from '../actions/bike';
import { ACTIONS as USER_ACTIONS } from '../actions/user';

export default (state = {} , action) => {
    const {type, payload, meta = {}} = action;
    switch (type) {
        case `${BIKE_ACTIONS.GET_LIST}_FULFILLED`: {
            const _state = {...state};
            (payload || []).forEach((bike) => {
                _state[bike.id] = {
                    ...bike,
                    photo: `${SERVER_URL}${bike.photo}`
                };
            })
            return _state;
        }
        case `${BIKE_ACTIONS.GET_AVAILABLE}_FULFILLED`: {
            const _state = {};
            (payload || []).forEach((bike) => {
                _state[bike.id] = {
                    ...bike,
                    photo: `${SERVER_URL}${bike.photo}`
                };
            });
            return _state;
        }
        case `${BIKE_ACTIONS.RATE}_FULFILLED`:
        case `${BIKE_ACTIONS.GET}_FULFILLED`: {
            return {
                ...state,
                [payload.id]: {
                    ...payload,
                    photo: `${SERVER_URL}${payload.photo}`
                }
            };
        }
        case `${BIKE_ACTIONS.CREATE}_FULFILLED`: {
            return {
                ...state,
                [payload.id]: {
                    ...payload,
                    photo: `${SERVER_URL}${payload.photo}`
                }
            };
        }
        case `${BIKE_ACTIONS.EDIT}_FULFILLED`: {
            return {
                ...state,
                [meta.bike.id]: {
                    ...state[meta.bike.id],
                    ...meta.bike,
                    photo: meta.bike.photo.uri || meta.bike.photo
                }
            };
        }
        case `${BIKE_ACTIONS.DELETE}_FULFILLED`: {
            const _state = {...state};
            delete _state[meta.id]
            return _state;
        }
        case `${USER_ACTIONS.LOGOUT}_FULFILLED`: {
            return {};
        }
        default: return state;
    }
};