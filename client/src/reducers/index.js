import {combineReducers} from 'redux';
import userReducer from './user'
import bikeReducer from './bike'
import rentalsReducer from './rentals'
import userListReducer from './userList'

export default combineReducers({
    user: userReducer,
    bikes: bikeReducer,
    users: userListReducer,
    rentals: rentalsReducer,
})