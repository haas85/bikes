import {ACTIONS as USER_ACTIONS} from '../actions/user';

export default (state = {} , action) => {
    const {type, payload, meta = {}} = action;
    switch (type) {
        case `${USER_ACTIONS.GET_LIST}_FULFILLED`: {
            const _state = {...state};
            (payload || []).forEach((user) => {
                _state[user.id] = user;
            })
            return _state;
        }
        case `${USER_ACTIONS.GET}_FULFILLED`: {
            return {
                ...state,
                [payload.id]: payload
            };
        }
        case `${USER_ACTIONS.CREATE}_FULFILLED`: {
            return {
                ...state,
                [payload.id]: payload
            };
        }
        case `${USER_ACTIONS.EDIT}_FULFILLED`: {
            return {
                ...state,
                [meta.user.id]: {
                    ...state[meta.user.id],
                    ...meta.user
                }
            };
        }
        case `${USER_ACTIONS.DELETE}_FULFILLED`: {
            const _state = {...state};
            delete _state[meta.id]
            return _state;
        }
        case `${USER_ACTIONS.LOGOUT}_FULFILLED`: {
            return {};
        }
        default: return state;
    }
};