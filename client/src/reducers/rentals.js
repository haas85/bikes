import {ACTIONS as RENTAL_ACTIONS} from '../actions/rentals';
import { ACTIONS as USER_ACTIONS } from '../actions/user';
import { SERVER_URL } from '../lib/api';

export default (state = {} , action) => {
    const {type, payload, meta = {}} = action;
    switch (type) {
        case `${RENTAL_ACTIONS.GET_LIST}_PENDING`: {
            return {};
        }
        case `${RENTAL_ACTIONS.GET_LIST}_FULFILLED`: {
            const _state = {};
            (payload || []).forEach((rental) => {
                _state[rental.id] = {
                    ...rental,
                    bike: {
                        ...rental.bike,
                        photo: `${SERVER_URL}${rental.bike.photo}`
                    }
                };
            })
            return _state;
        }
        case `${RENTAL_ACTIONS.GET}_FULFILLED`: {
            return {
                ...state,
                [payload.id]: {
                    ...payload,
                    bike: {
                        ...payload.bike,
                        photo: `${SERVER_URL}${payload.bike.photo}`
                    }
                }
            };
        }
        case `${RENTAL_ACTIONS.DELETE}_FULFILLED`: {
            const _state = {...state};
            delete _state[meta.id]
            return _state;
        }
        case `${USER_ACTIONS.LOGOUT}_FULFILLED`: {
            return {};
        }
        default: return state;
    }
};