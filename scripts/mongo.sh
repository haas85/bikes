#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
SCRIPTPATH=`dirname $SCRIPTPATH`
SCRIPTPATH="$SCRIPTPATH/docker-compose.yml"

docker image list | grep 'bikes_mongo' &> /dev/null
if [ $? == 0 ]; then
   echo "Mongo image exists, if you want to rebuild it execute docker-compose build mongo"
else
    docker-compose -f $SCRIPTPATH build mongo
fi

if [ "$1" == "test" ]; then
    docker-compose -f $SCRIPTPATH run --rm --service-ports mongotest
else
    docker-compose -f $SCRIPTPATH run --rm --service-ports mongo
fi