global.winston = require('winston');
const Mongo = require('./lib/mongo');
const Server = require('./lib/server');

process.on('unhandledRejection', (reason, p) => {
    if (reason.stack) {
        winston.log('error', reason.stack);
    } else {
        winston.log('error', 'Unhandled Rejection at: Promise ', p, ' reason: ', reason);
    }
});

module.exports = Mongo.connect()
    .then(() => {
        winston.log('info', 'Properly connected to mongo');
        Server.createServer();
    })
    .catch((err) => {
        winston.log('error', 'Error connecting to mongo', err);
        throw err;
    });