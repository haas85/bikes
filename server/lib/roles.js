const ROLES = {
    user: 'USER',
    manager: 'MANAGER'
};

module.exports = {
    ROLES
};