const bcrypt = require('bcrypt');

const saltRounds = 10;

async function createHash(password) {
    const hash = await bcrypt.hash(password, saltRounds);
    return hash;
}

async function verifyHash(hash, password) {
    const match = await bcrypt.compare(password, hash);
    return match;
}

module.exports = {
    create: createHash,
    verify: verifyHash
};