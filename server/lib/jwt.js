const jwt = require('jsonwebtoken');
const secret = process.env.SECRET || 'secret';

const sign = (data) => jwt.sign(data, secret, { expiresIn: '200 years' });

const verify = (token) => new Promise((resolve, reject) => {
    jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            reject(err);
        } else {
            resolve(decoded);
        }
    })
});

module.exports = {
    sign,
    verify
};
