const parseDuplicate = (err) => {
    let matches = err && err.message.match(/^E11000 duplicate key error (index|collection): (?:[a-z-.]+\.)+([a-z]*) index\: ([a-z]+)_([0-9]+)/);
    if (matches) {
        return { error: 'EXISTING', meta: matches[3] };
    } else {
        matches = err && err.message.match(/E11000 duplicate key error index: (?:[a-z-_]+\.)+\$_?([a-z]+)/);
        if (matches) {return {error: 'EXISTING', meta: matches[1]}; }
    }
    return err;
};

const setErrorKey = (error) => {
    switch(error.toUpperCase()) {
        case 'VALIDATORERROR':
            return 'WRONG_FORMAT';
        default:
            console.error(error);
            return error.toUpperCase();
    }
}

const parseErrors = (err) => {
    let errorKeys = Object.keys(err.errors);
    const errors = {};
    for (let i = 0, len = errorKeys.length; i < len; i++) {
        const error = err.errors[errorKeys[i]];
        const key = setErrorKey(error.name);
        errors[key] = errors[key] || [];
        errors[key].push(error.path);
    }
    errorKeys = Object.keys(errors);
    if (errorKeys.length === 1) {
        return { error: errorKeys[0], meta: errors[errorKeys[0]].join(',') }
    }
    const finalError = { error: 'MULTIPLE', meta: [] };
    for (let i = 0, len = errorKeys.length; i < len; i++) {
        finalError.meta.push({ error: errorKeys[i], meta: errors[errorKeys[i]].join(',') });
    }
    return finalError;
};

const errors = {
    E11000: parseDuplicate
};

const errorKeys = Object.keys(errors);

const parse = (err) => {
    if (typeof err === 'string') {
        return { error: err };
    }
    if (err.errors) {
        return parseErrors(err);
    }
    for (let i = 0, len = errorKeys.length; i < len; i++) {
        if (err.message.indexOf(errorKeys[i]) !== -1) {
            return errors[errorKeys[i]](err);
        }
    }
    return { error: 'UNHANDLED', meta: err.message };
};

module.exports = parse;