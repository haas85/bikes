const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const config = require('../../config');

const mongo = config.mongo;

const connect = () => {
    mongoose.set('useCreateIndex', true);
    mongoose.set('useFindAndModify', false);
    return mongoose.connect(`mongodb://${mongo.user}:${mongo.pass}@${mongo.host}:${mongo.port}/${mongo.db}`, { useNewUrlParser: true });
}

module.exports = {
    connect
};
