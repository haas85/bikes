const jwt = require('./jwt');

const pad = (n, width, z) => {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

const time2Number = (time) => parseInt(`${time}`.replace(':', ''), 10);
const number2Time = (number) => {
    const padded = `${pad(number, 4)}`;
    return `${padded[0]}${padded[1]}:${padded[2]}${padded[3]}`;
};

const setSession = (ctx, user) => {
    const token = jwt.sign({id: user.id, mail: user.mail});
    ctx.cookies.set('bike-session', token, { maxAge: 24 * 60 * 60 * 10000000 });
    return token;
}

module.exports = {
    pad,
    time2Number,
    number2Time,
    setSession
};
