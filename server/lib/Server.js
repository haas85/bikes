const fs = require('fs');
const path = require('path');
const Koa = require('koa');
const logger = require('koa-logger');
const koaBody = require('koa-body');
const views = require('koa-views');
const serve = require('koa-static');
const hbs = require('handlebars');

const constants = require('./constants')
const router = require('../routes');
const notFound = require('../middleware/404');
const jwt = require('../middleware/validate');
const queryParser = require('../middleware/queryParser');

let server;

const createServer = (port) => {
    const app = module.exports = new Koa();


    app.use(logger());

    hbs.registerHelper('json', function(context) {
        return JSON.stringify(context);
    });

    app.use(views(path.join(__dirname, '../views'), {
        extension: 'hbs',
        map: { hbs: 'handlebars' }
    }));
    app.use(koaBody());
    app.use(queryParser);
    app.use(jwt.verifier);

    app.use(serve(path.join(__dirname, '../dist')));
    console.log(path.join(__dirname, '../dist'));
    const imgPath = path.join(__dirname, `../dist/${constants.IMG_PATH}`)
    try {
        if (!fs.existsSync(imgPath)){
            fs.mkdirSync(imgPath);
        }
    } catch(e) {}

    app.use(router.routes());
    app.use(notFound);

    server = app.listen(port || process.env.PORT || 3010);
    winston.log('info', `Server listening at ${process.env.PORT || 3010}`);
    return server;
};

const getServer = () => server;

const stop = () => server.close();

// middleware
module.exports = {
    createServer,
    getServer,
    stop
};
