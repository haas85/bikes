const deepmerge = require('deepmerge');

var config = {
    default: {
        mongo: {
            host: process.env.MONGO_HOST || 'localhost',
            port: process.env.MONGO_PORT || 27017,
            db: process.env.MONGO_DB || 'bikes',
            user: process.env.MONGO_USER || 'bikes_admin',
            pass: process.env.MONGO_PASS || 'password'
        }
    },
    test: {
        mongo: {
            host: 'localhost',
            port: 27018,
            db: 'bikes',
            user: 'bikes_admin',
            pass: 'password'
        }
    },
    stage: {
        mongo: {
            host: process.env.MONGO_HOST,
            port: process.env.MONGO_PORT,
            db: process.env.MONGO_DB,
            user: process.env.MONGO_USER,
            pass: process.env.MONGO_PASS
        }
    },
    prod: {
        mongo: {
            host: process.env.MONGO_HOST,
            port: process.env.MONGO_PORT,
            db: process.env.MONGO_DB,
            user: process.env.MONGO_USER,
            pass: process.env.MONGO_PASS
        }
    }
}

module.exports = deepmerge(config.default, config[process.env.NODE_ENV] || {});