const qs = require('query-string');

module.exports = async (ctx, next) => {
    ctx.request.query = qs.parse(ctx.request.querystring || '');
    return next();
};
