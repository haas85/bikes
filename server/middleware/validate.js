const jwt = require('../lib/jwt');
const UserModel = require('../models/user')

const verifier = async (ctx, next) => {
    const token = ctx.cookies.get('bike-session') || ctx.request.headers['Authorization'];
    if (token) {
        try {
            const decoded = await jwt.verify(token);
            const user = await UserModel.findById(decoded.id);
            if (!user) {
                throw 'NOT_VALID'
            }
            ctx.user = {
                id: user._id,
                mail: user.mail,
                role: user.role,
            };
            return next();
        } catch(e) {
            ctx.status = 401;
            ctx.body = { error: 'NOT_VALID_USER' };
            ctx.cookies.set('bike-session', '', { maxAge: 1 });
            return;
        }
    }
    return next();
};



module.exports = {
    verifier
};
