module.exports = (params = []) => {
    return (ctx, next) => {
        const reqParams = [].concat(Object.keys(ctx.request.query), Object.keys(ctx.request.body), Object.keys(ctx.request.files || {}));
        const missingParams = [];
        for (let i = 0, len = params.length; i < len; i++) {
            if (reqParams.indexOf(params[i]) === -1) {
                missingParams.push(params[i])
            }
        }
        if (missingParams.length !== 0) {
            ctx.status = 400;
            ctx.body = { error: 'PARAM_MISSING', meta: missingParams.join(',') };
        } else {
            return next();
        }
    };
};
