async function pageNotFound(ctx) {
    // we need to explicitly set 404 here
    // so that koa doesn't assign 200 on body=
    ctx.status = 404;

    switch (ctx.accepts('json', 'html')) {
        case 'html':
            await ctx.render('404');
            break;
        case 'json':
            ctx.body = {
                error: 'NOT_FOUND'
            };
            break;
        default:
            ctx.type = 'text';
            ctx.body = 'Page Not Found';
    }
};

module.exports = pageNotFound;