const errorParser = (e) => {
    return {
        error: e.code,
        meta: e.field
    };
};


module.exports = (fn) =>
    async(ctx, next) => {
        try {
            await fn(ctx, () => {});
            ctx.request.body = ctx.req.body;
            ctx.request.files = ctx.req.files;
            await next();
        } catch(e) {
            ctx.status = 400;
            ctx.body = errorParser(e);
        }
    }