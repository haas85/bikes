module.exports = (roles) =>async (ctx, next) => {
    if (ctx.user) {
        if (!roles || roles.includes(ctx.user.role)) {
            return next();
        } else {
            ctx.status = 403;
            ctx.body = { error: 'FORBIDDEN' };
        }
    } else {
        switch (ctx.accepts('json', 'html')) {
            case 'html': {
                const baseUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:3000/login' : '';
                await ctx.redirect(`${baseUrl}/?origin=${encodeURIComponent(ctx.request.url)}`);
                break;
            }
            case 'json':
                ctx.status = 401;
                ctx.body = { error: 'NOT_LOGGED' };
                break;
            default:
                ctx.status = 401;
                ctx.body = { error: 'NOT_LOGGED' };
        }
    }
};
