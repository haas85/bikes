const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ratingSchema = new Schema({
    user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    bike: { type: Schema.Types.ObjectId, required: true, ref: 'Bike' },
    rental: { type: Schema.Types.ObjectId, required: true, ref: 'Rental' },
    rating: { type: Number, required: true, min: 1, max: 5 },
    created: { type: Date, default: Date.now }
});

const transform = (doc, ret, options = {}) => {
    delete ret.__v;
    return ret;
};

ratingSchema.set('toJSON', {
    getters: true,
    transform
});

ratingSchema.set('toObject', {
    getters: true,
    transform
});

const createModel = () => mongoose.model('Rating', ratingSchema);

ratingSchema.statics.create = function(data) {
    const Rating = createModel();
    Rating.find({
        user: data.user,
        bike: data.bike,
        rental: data.rental,
    }).then((results) => {
        if (results && results[0]) {
            return results[0].updateOne({
                ...data,
                rating: data.rating || this.rating,
            }).then((rating) =>
                require('./bike').updateAverage(data.bike).then(() =>
                    rating
                ));
        } else {
            return new Rating(data).save().then((rating) =>
                require('./bike').updateAverage(data.bike).then(() =>
                    rating
                )
            );
        }
    });
};

ratingSchema.methods.delete = function() {
    // Add here more delete logic
    const bike = this.bike;
    return this.remove().then(function() {
        return require('./bike').updateAverage(bike);
    });
}

module.exports = createModel();
