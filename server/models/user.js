const mongoose = require('mongoose');

const hash = require('../lib/hash');
const ROLES = require('../lib/roles').ROLES;

const RentalModel = require('./rental');
const RatingModel = require('./rating');

const Schema = mongoose.Schema;

const mailRegex = /(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

const userSchema = new Schema({
    mail: { type: String, unique: true, minlength: 6, maxlength: 50, match: mailRegex },
    password: { type: String, minlength: 6 },
    role: { type: String, enum: Object.values(ROLES), default: ROLES.user},
});

const transform = (doc, ret, options = {}) => {
    delete ret.password;
    delete ret.__v;
    delete ret._id;
    return ret;
};

userSchema.set('toJSON', {
    getters: true,
    transform
});

userSchema.set('toObject', {
    getters: true,
    transform
});

const createModel = () => mongoose.model('User', userSchema);

userSchema.statics.create = async function(data) {
    const userData = {
        mail: data.mail.replace(/\s/g,'').toLowerCase(),
        role: data.role
    };
    if (data.password.length >= 6) {
        userData.password = await hash.create(data.password);
    } else {
        userData.password = data.password;
    }
    const UserModel = createModel();
    return new UserModel(userData).save();
};

userSchema.statics.login = function(mail, password) {
    return this.findOne({
        mail: mail.toLowerCase()
    })
        .then(async (user) => {
            if (user) {
                const valid = await hash.verify(user.password, password)
                if (valid) {
                    return user.toObject();
                }
                throw 'NOT_VALID';
            }
        });
};

userSchema.statics.edit = function(id, {mail, password, role}) {
    return this.findById(id).then(async (user) => {
        const userData = {
            mail: (mail || user.mail).replace(/\s/g,'').toLowerCase(),
            role: role || user.role,
        };
        if (password) {
            if (password.length >= 6) {
                userData.password = await hash.create(password);
            } else {
                throw 'Invalid Password'
            }
        }
        return user.updateOne(userData);
    });
};

userSchema.methods.delete = function() {
    const id = this._id;
    return this.remove().then(() =>
        Promise.all([
            RentalModel.deleteMany({user: id}),
            RatingModel.deleteMany({user: id})
        ])
    );
}

module.exports = createModel();
