const mongoose = require('mongoose');
const RentalModel = require('./rental');
const RatingModel = require('./rating');

const Schema = mongoose.Schema;

const bikeSchema = new Schema({
    model: { type: String, required: true , minlength: 2, maxlength: 50 },
    photo: { type: String, required: true , minlength: 2, maxlength: 50 },
    color: { type: String, required: true , minlength: 2, maxlength: 50 },
    weight: { type: Number, required: true, min: 0 },
    location: { type: Object, required: true  },
    available: { type: Boolean, default: true },
    average: { type: Number, min: 1, max: 5 },
    created: { type: Date, default: Date.now }
});

const transform = (doc, ret, options = {}) => {
    delete ret.__v;
    return ret;
};

bikeSchema.set('toJSON', {
    getters: true,
    transform
});

bikeSchema.set('toObject', {
    getters: true,
    transform
});

const createModel = () => mongoose.model('Bike', bikeSchema);


bikeSchema.statics.create = function(data) {
    const BikeModel = createModel();
    return new BikeModel(data).save();
};

bikeSchema.methods.edit = function(model, photo, color, weight, location, available) {
    return this.updateOne({
        model: model || this.model,
        photo: photo || this.photo,
        color: color || this.color,
        weight: weight || this.weight,
        location: location || this.location,
        available: available !== undefined ? available : this.available
    });
}

bikeSchema.statics.updateAverage = function(id) {
    return RatingModel.find({ bike: id}).select('rating -_id').then(function(ratings) {
        const value = ratings.reduce((acc, rev) => {
            return acc + rev.rating;
        }, 0);
        const average = value === 0 ? undefined : parseInt(value / ratings.length, 10);
        return createModel().findOneAndUpdate({_id: id}, { average });
    })
}

bikeSchema.methods.delete = function() {
    const id = this._id;
    return this.remove().then(() =>
        Promise.all([
            RentalModel.deleteMany({bike: id}),
            RatingModel.deleteMany({bike: id})
        ])
    );
}

module.exports = createModel();
