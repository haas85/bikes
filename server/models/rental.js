const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const rentalSchema = new Schema({
    user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    bike: { type: Schema.Types.ObjectId, required: true, ref: 'Bike' },
    initial: { type: Date, required: true },
    end: { type: Date, required: true },
    created: { type: Date, default: Date.now }
});

const transform = (doc, ret, options = {}) => {
    delete ret.__v;
    return ret;
};

rentalSchema.set('toJSON', {
    getters: true,
    transform
});

rentalSchema.set('toObject', {
    getters: true,
    transform
});

const createModel = () => mongoose.model('Rental', rentalSchema);


rentalSchema.statics.create = function(data) {
    const RentalModel = createModel();
    data.initial = moment(data.initial).startOf('day').toDate();
    data.end = moment(data.end).startOf('day').toDate();
    return new RentalModel(data).save();
};

rentalSchema.methods.delete = function() {
    return this.remove();
}

module.exports = createModel();
