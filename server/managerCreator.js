const Mongo = require('./lib/mongo');
const hash = require('./lib/hash');
const roles = require('./lib/roles');
const {STATUS} = require('./lib/status');
const UserModel = require('./models/user');

const args = process.argv.slice(2);

const createUser = async function(data) {
    const userData = {
        mail: data.mail.replace(/\s/g, '').toLowerCase(),
        role: data.role
    };
    if (data.password.length >= 6) {
        userData.password = await hash.create(data.password);
    } else {
        userData.password = data.password;
    }
    if (data.status) {
        userData.status = data.status;
    }
    return new UserModel(userData).save();
}

if (args.length !== 2) {
    console.log('Add mail and password as arguments')
} else {
    const mail = args[0];
    const password = args[1];
    Mongo.connect()
        .then(() => {
            console.log('Properly connected to mongo');
            createUser({
                mail,
                password,
                role: roles.ROLES.manager,
                status: STATUS.READY
            })
                .then(() => {
                    console.log('Admin created');
                    process.exit(0);
                })
                .catch((error) => {
                    console.log('Error creating', error);
                    process.exit(1);
                });
        })
        .catch((err) => {
            console.log('Error connecting to mongo', err);
            process.exit(1);
        });
}