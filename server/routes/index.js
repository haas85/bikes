const router = require('koa-router')();
const api = require('./api');

router.use('/api', api.routes());

module.exports = router;