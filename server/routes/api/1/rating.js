const router = require('koa-router')();

const protect = require('../../../middleware/protected');
const paramCheck = require('../../../middleware/paramCheck');
const mongoParser = require('../../../lib/mongo/errorParser');

const RatingModel = require('../../../models/rating');
const BikeModel = require('../../../models/bike');

async function getRating(ctx, next) {
    let bike;
    try {
        bike = await BikeModel.findById(ctx.query.bike);
        // Return 404 in case no entry or the entry doesn't belong to non admin user
        if (!bike) {
            throw 'NOT_FOUND';
        }
    } catch(err) {
        ctx.status = 500;
        ctx.body = {error: 'SERVER_ERROR'};
        return;
    }

    try {
        const ratings = await RatingModel.find({bike: ctx.query.bike})
        ctx.body = ratings.map((rating) => rating.toObject());
    } catch (err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}

async function createRating(ctx, next) {
    let bike;
    try {
        bike = await BikeModel.findById(ctx.params.bike);
        // Return 404 in case no entry or the entry doesn't belong to non admin user
        if (!bike) {
            throw 'NOT_FOUND';
        }
    } catch(err) {
        ctx.status = 404;
        ctx.body = {
            error: 'NOT_FOUND'
        };
        return;
    }

    try {
        const data = {
            user: ctx.user.id,
            bike: ctx.params.bike,
            rating: ctx.request.body.rating,
            rental: ctx.request.body.rental
        }
        await RatingModel.create(data);
        ctx.status = 201;
        ctx.body = {
            status: 'ok'
        };
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}

router.get('/:id', protect(), getRating);
router.post('/:bike', protect(), paramCheck(['rating', 'rental']), createRating);

module.exports = router;
