const router = require('koa-router')();
const moment = require('moment');

const protect = require('../../../middleware/protected');
const paramCheck = require('../../../middleware/paramCheck');
const mongoParser = require('../../../lib/mongo/errorParser');
const roles = require('../../../lib/roles');

const RentalModel = require('../../../models/rental');
const BikeModel = require('../../../models/bike');

async function getRentals(ctx, next) {
    let rentals;
    try {
        if (ctx.user.role === roles.ROLES.manager) {
            const filter = {};
            if (ctx.query.user) {
                filter.user = ctx.query.user;
            }
            if (ctx.query.bike) {
                filter.bike = ctx.query.bike;
            }
            rentals = await RentalModel.find(filter).populate('bike').populate('user');
        } else {
            rentals = await RentalModel.find({user: ctx.user.id}).populate('bike');
        }
        ctx.body = rentals.map((rental) => rental.toObject());
    } catch (err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}

async function createRental(ctx, next) {
    let bike;
    try {
        bike = await BikeModel.findById(ctx.request.body.bike);
        // Return 404 in case no entry or the entry doesn't belong to non admin user
        if (!bike) {
            throw 'NOT_FOUND';
        }
    } catch(err) {
        ctx.status = 404;
        ctx.body = {
            error: 'NOT_FOUND'
        };
        return;
    }

    const today = moment().startOf('day').toDate();

    const initial = moment(parseInt(ctx.request.body.initial, 10)).startOf('day').toDate();
    const end = moment(parseInt(ctx.request.body.end, 10)).startOf('day').toDate();

    if (initial < today) {
        ctx.status = 400;
        ctx.body = {
            error: 'WRONG_START_DATE'
        };
        return;
    }

    if (initial > end) {
        ctx.status = 400;
        ctx.body = {
            error: 'WRONG_RANGE'
        };
        return;
    }

    try {
        const filters = {
            bike: ctx.request.body.bike,

            $or: [
                {
                    initial: {
                        $gte: initial,
                        $lte: end
                    }
                },
                {
                    end: {
                        $gte: initial,
                        $lte: end
                    }
                },
                {
                    initial: {$lte: initial},
                    end: {$gte: initial},
                },
                {
                    end: {$gte: end},
                    initial: {$lte: end},
                }
            ]
        };
        const rentals = await RentalModel.find(filters);

        if (rentals && rentals.length > 0) {
            throw 'CONFLICT';
        }
    } catch (err) {
        ctx.status = 409;
        ctx.body = {
            error: 'CONFLICT'
        };
        return;
    }

    try {
        const data = {
            user: ctx.user.id,
            bike: ctx.request.body.bike,
            initial: initial,
            end: end,
        }
        await RentalModel.create(data);
        ctx.status = 201;
        ctx.body = {
            status: 'ok'
        };
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}


async function removeRental(ctx, next) {
    let rental;
    try {
        rental = await RentalModel.findById(ctx.params.id);
    } catch(err) {
        return next();
    }
    if (rental.user.toString() !== ctx.user.id.toString() && (ctx.user.role !== roles.ROLES.manager)) {
        ctx.status = 403;
        ctx.body = {
            error: 'FORBIDDEN'
        };
        return;
    }
    try {
        await rental.delete();
        ctx.body = {
            status: 'Ok'
        };
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}


router.get('/', protect(), getRentals);
router.post('/', protect(), paramCheck(['bike', 'initial', 'end']), createRental);
router.del('/:id', protect(), removeRental);

module.exports = router;
