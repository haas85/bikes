const path = require('path');
const fs = require('fs');
const uuidv4 = require('uuid/v4');
const router = require('koa-router')();
const moment = require('moment');
const multer = require('koa-multer');
const constants = require('../../../lib/constants');

const roles = require('../../../lib/roles');

const protect = require('../../../middleware/protected');
const multipart = require('../../../middleware/multipart');
const paramCheck = require('../../../middleware/paramCheck');
const mongoParser = require('../../../lib/mongo/errorParser');

const BikeModel = require('../../../models/bike');
const RentalModel = require('../../../models/rental');

const upload =  multer({
    limits: {
        fileSize: 2 * 1024 * 1024
    },
    dest: 'uploads/'
});

const processPhoto = (ctx) => {
    const photo = ctx.request.files.photo[0];
    if (photo.mimetype.indexOf('image') === -1) {
        ctx.status = 400;
        ctx.body = {
            error: 'INVALID_FILE_TYPE',
        };
        return;
    }
    const fileName = `${uuidv4()}${path.extname(photo.originalname)}`;
    const imgPath = path.join(__dirname, `../../../dist/${constants.IMG_PATH}`, fileName);
    fs.copyFile(photo.path, imgPath, (err) => {
        fs.unlink(photo.path, () => {});
        if (err) {
            ctx.status = 500;
            ctx.body = {error: 'SERVER_ERROR'};
            return;
        }
    });

    return `/${constants.IMG_PATH}/${fileName}`;
}


const hasOwnProperty = (object, prop) => Object.prototype.hasOwnProperty.call(object, prop);

async function getBikes(ctx) {
    try {
        const bikes = await BikeModel.find({});
        ctx.body = bikes.map((bike) => bike.toObject());
    } catch(err) {
        ctx.status = 500;
        ctx.body = {error: 'SERVER_ERROR'};
    }
}

async function getAvailableBikes(ctx) {
    try {
        const filters = {
        };
        const {query} = ctx;

        if (hasOwnProperty(query, 'fromDate') && hasOwnProperty(query, 'toDate') && query.toDate < query.fromDate) {
            ctx.status = 400;
            ctx.query = {error: 'WRONG_RANGE'};
            return;
        }

        if (hasOwnProperty(query, 'fromDate') && hasOwnProperty(query, 'toDate')) {
            const fromDate = moment(parseInt(query.fromDate, 10)).startOf('day').toDate();
            const toDate = moment(parseInt(query.toDate, 10)).startOf('day').toDate();
            filters['$or'] = [
                {
                    initial: {
                        $gte: fromDate,
                        $lte: toDate
                    }
                },
                {
                    end: {
                        $gte: fromDate,
                        $lte: toDate
                    }
                },
                {
                    initial: {$lte: fromDate},
                    end: {$gte: fromDate},
                },
                {
                    end: {$gte: toDate},
                    initial: {$lte: toDate},
                }
            ];
        }


        const rentedBikes = await RentalModel.find(filters).select('bike -_id');

        const availableBikes = await BikeModel.find({available: true, _id: { $nin: rentedBikes.map(({bike}) => bike)}});
        ctx.body = availableBikes.map((bike) => bike.toObject());
    } catch(err) {
        ctx.status = 500;
        ctx.body = {error: 'SERVER_ERROR'};
    }
}

async function getBike(ctx, next) {
    try {
        const bike = await BikeModel.findById(ctx.params.id);
        // Return 404 in case no entry or the entry doesn't belong to non admin user
        if (!bike) {
            throw 'NOT_FOUND';
        }
        ctx.body = bike.toObject();
    } catch(err) {
        ctx.status = 404;
        ctx.body = {
            error: 'NOT_FOUND'
        };
    }
}

async function createBike(ctx, next) {
    const data = {
        model: ctx.request.body.model,
        location: JSON.parse(ctx.request.body.location),
        color: ctx.request.body.color,
        weight: ctx.request.body.weight
    }
    const photo = processPhoto(ctx);
    if (!photo) {
        return;
    } else {
        data.photo = photo;
    }
    try {
        const entry = await BikeModel.create(data);
        ctx.status = 201;
        ctx.body = entry.toObject();
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}

async function setBike(ctx, next) {
    let bike;
    try {
        bike = await BikeModel.findById(ctx.params.id);
    } catch(err) {
        return next();
    }
    const data = {...bike.toObject()};
    if (ctx.request.body.model) {
        data.model = ctx.request.body.model;
    }
    if (ctx.request.body.location && location !== '') {
        try {
            data.location = JSON.parse(ctx.request.body.location);
        } catch (e) {

        }
    }
    if (ctx.request.body.color) {
        data.color = ctx.request.body.color;
    }

    if (ctx.request.body.weight) {
        data.weight = ctx.request.body.weight;
    }

    if (hasOwnProperty(ctx.request.body, 'available')) {
        data.available = ctx.request.body.available === 'true';
    }


    if (ctx.request.files.photo && ctx.request.files.photo[0]) {
        const photo = processPhoto(ctx);
        if (!photo) {
            return;
        } else {
            data.photo = photo;
        }
    }

    try {
        await bike.edit(data.model, data.photo, data.color, data.weight, data.location, data.available);
        ctx.body = {
            status: 'Ok'
        };
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}

async function removeBike(ctx, next) {
    let entry;
    try {
        entry = await BikeModel.findById(ctx.params.id);
    } catch(err) {
        return next();
    }
    try {
        await entry.delete();
        ctx.body = {
            status: 'Ok'
        };
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = 400;
        ctx.body = error;
    }
}

router.get('/available', protect(), paramCheck(['fromDate', 'toDate']), getAvailableBikes);
router.get('/:id', protect(), getBike);
router.get('/', protect(), getBikes);
router.post('/', protect([roles.ROLES.manager]), multipart(upload.fields([{ name: 'photo', maxCount: 1 }])), paramCheck(['model', 'location', 'color', 'weight', 'photo']), createBike);
router.put('/:id', protect([roles.ROLES.manager]), multipart(upload.fields([{ name: 'photo', maxCount: 1 }])), setBike);
router.del('/:id', protect([roles.ROLES.manager]), removeBike);

module.exports = router;
