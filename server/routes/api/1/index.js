const api = require('koa-router')();
const userRouter = require('./user');
const bikeRouter = require('./bike');
const ratingRouter = require('./rating');
const rentalRouter = require('./rental');

api.use('/user', userRouter.routes());
api.use('/bike', bikeRouter.routes());
api.use('/rating', ratingRouter.routes());
api.use('/rental', rentalRouter.routes());

module.exports = api;