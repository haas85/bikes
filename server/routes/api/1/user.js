const router = require('koa-router')();
const validator = require('validator');

const utils = require('../../../lib/utils');
const roles = require('../../../lib/roles');

const protect = require('../../../middleware/protected');
const paramCheck = require('../../../middleware/paramCheck');

const mongoParser = require('../../../lib/mongo/errorParser');

const UserModel = require('../../../models/user');

async function getUsers(ctx, next) {
    try {
        const users = await UserModel.find({});
        ctx.body = users.map((user) => user.toObject());
    } catch(e) {
        return next();
    }
}

async function getUser(ctx, next) {
    const userId = ctx.params.id || ctx.user.id;
    try {
        const user = await UserModel.findOne({ _id: userId });
        ctx.body = user.toObject();
    } catch(e) {
        return next();
    }
}

async function create(ctx) {
    if (![roles.ROLES.user, roles.ROLES.manager].includes(ctx.request.body.role || 'USER')) {
        ctx.status = 400;
        ctx.body = {error: 'INVALID_ROLE'};
        return;
    }
    try {
        let user = await UserModel.create(ctx.request.body);
        ctx.body = user.toObject();
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = error.error === 'EXISTING' ? 409 : 400;
        ctx.body = error;
    }
}

async function setUser(ctx) {
    if (ctx.request.body.mail && !validator.isEmail(ctx.request.body.mail)) {
        ctx.status = 400;
        ctx.body = { error: 'WRONG_FORMAT', meta: 'mail' };
        return
    }
    if (ctx.request.body.role && ![roles.ROLES.user, roles.ROLES.manager].includes(ctx.request.body.role)) {
        ctx.status = 400;
        ctx.body = { error: 'WRONG_FORMAT', meta: 'role' };
        return
    }
    const userId = ctx.params.id || ctx.user.id;
    if (userId !== ctx.user.id && (ctx.user.role !== roles.ROLES.manager)) {
        ctx.status = 403;
        ctx.body = {error: 'FORBIDDEN'};
    } else {
        try {
            const data = {
                mail: ctx.request.body.mail,
                role: ctx.request.body.role,
            };
            await UserModel.edit(userId, data);
            ctx.body = {
                status: 'Ok'
            };
        } catch (err) {
            const error = mongoParser(err);
            ctx.status = error.error === 'EXISTING' ? 409 : 400;
            ctx.body = error;
        }
    }
}

async function login(ctx) {
    try {
        const user = await UserModel.login(ctx.request.body.mail, ctx.request.body.password);
        const token = utils.setSession(ctx, user);
        ctx.body = {
            ...user,
            token
        };
    } catch(err) {
        ctx.status = 401;
        ctx.body = {error: 'INVALID_USER_PASSWORD'};
    }
}
async function logout(ctx) {
    if (ctx.user) {
        ctx.cookies.set('bike-session', '', { maxAge: 1 });
        ctx.body = {
            status: 'Ok'
        };
    } else {
        ctx.status = 401;
        ctx.body = {error: 'NOT_LOGGED'};
    }
}

async function signup(ctx) {
    if (![roles.ROLES.user].includes(ctx.request.body.role || 'USER')) {
        ctx.status = 400;
        ctx.body = {error: 'INVALID_ROLE'};
        return;
    }
    try {
        let user = await UserModel.create(ctx.request.body);
        ctx.body = {
            status: 'Ok'
        };
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = error.error === 'EXISTING' ? 409 : 400;
        ctx.body = error;
    }
}

async function delUser(ctx, next) {
    let user;
    try {
        user = await UserModel.findById(ctx.params.id)
    } catch(err) {
        return next();
    }
    try {
        await user.delete();
        ctx.body = {
            status: 'Ok'
        };
    } catch(err) {
        const error = mongoParser(err);
        ctx.status = error.error === 'EXISTING' ? 409 : 400;
        ctx.body = error;
    }
}


router.get('/', protect(), getUser);
router.get('/:id', protect([roles.ROLES.manager]), getUser);
router.get('/all', protect([roles.ROLES.manager]), getUsers);
router.post('/create', protect([roles.ROLES.manager]), paramCheck(['mail', 'password', 'role']), create);
router.put('/:id', protect([roles.ROLES.manager]), setUser);
router.put('/', protect(), setUser);
router.del('/:id', protect([roles.ROLES.manager]), delUser);

router.post('/login', paramCheck(['mail', 'password']), login);
router.post('/logout', protect(), logout);
router.post('/signup', paramCheck(['mail', 'password']), signup);


module.exports = router;