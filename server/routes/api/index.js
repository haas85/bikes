const api = require('koa-router')();
const v1Router = require('./1');

api.use('/1', v1Router.routes());


module.exports = api;